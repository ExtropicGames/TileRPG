using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;

using TileRPG.Model;
using System.Diagnostics.Contracts;

namespace TileRPG.Shared
{
    public class Renderer
    {
        private Dictionary<string, Texture2D> textures;
        private const int TILE_SIZE = 16;
        private const float SCALE = 2f;



        public void Initialize(Dictionary<string, Texture2D> textures)
        {
            this.textures = textures;
        }

        public void Draw(Rectangle viewport, SpriteBatch spriteBatch, World world)
        {
            // TODO: for now, we just support one global map
            Map map = world.maps [0];

            for (int x = 0; x < map.tiles.GetLength(0); x++)
            {
                for (int y = 0; y < map.tiles.GetLength(1); y++)
                {

                    Point<int> spriteIndex = TileResolver.terrainIndexLookup(new List<Map.Terrain>() {
                        map.tile(x-1, y-1),
                        map.tile(x, y-1),
                        map.tile(x+1, y-1),
                        map.tile(x-1, y),
                        map.tile(x, y),
                        map.tile(x+1, y),
                        map.tile(x-1, y+1),
                        map.tile(x, y+1),
                        map.tile(x+1, y+1)
                    });

                    Rectangle clip = new Rectangle (spriteIndex.x * (TILE_SIZE + 1), spriteIndex.y * (TILE_SIZE + 1), TILE_SIZE, TILE_SIZE);
                    Vector2 position = new Vector2 (x * TILE_SIZE * SCALE, y * TILE_SIZE * SCALE);
                    if (map.tile(x, y) == Map.Terrain.Water)
                    {
                        spriteBatch.Draw (textures["waterTiles"], position, clip, Color.White, 0f, Vector2.Zero, SCALE, SpriteEffects.None, 0f);
                    }
                    else
                    {
                        spriteBatch.Draw (textures["tileset"], position, clip, Color.White, 0f, Vector2.Zero, SCALE, SpriteEffects.None, 0f);
                    }
                }
            }

            foreach (Creature c in map.creatures)
            {
                Rectangle clip = new Rectangle ((int) c.type * (TILE_SIZE + 1), 0, TILE_SIZE, TILE_SIZE);
                Vector2 position = new Vector2 (c.position.x * TILE_SIZE, c.position.y * TILE_SIZE);
                spriteBatch.Draw (textures["creatures"], position, clip, Color.White, 0f, Vector2.Zero, SCALE, SpriteEffects.None, 0f);
            }
        }
    }
}

