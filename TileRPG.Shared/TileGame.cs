﻿#region Using Statements
using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

using TileRPG.Model;
using System.Collections.Generic;

#endregion

namespace TileRPG.Shared
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class TileGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Renderer renderer;
        World world = new World ();

        public TileGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.IsFullScreen = true;
            Map home = new Map (80, 80);
            home.creatures.Add (new Creature (true));
            world.maps.Add (home);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            renderer = new Renderer ();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Reminder: If your content won't load, make sure you set the
            // BuildAction to "Content" and checked "Copy to Output Directory"
            Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D> {
                { "waterTiles", Content.Load<Texture2D> ("Graphics/waterland") },
                { "tileset", Content.Load<Texture2D> ("Graphics/tileset") },
                { "creatures", Content.Load<Texture2D> ("Graphics/characters") },
            };

            renderer.Initialize (textures);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // For Mobile devices, this logic will close the Game when the Back button is pressed
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                Exit();
            }
            if (Keyboard.GetState ().IsKeyDown (Keys.Escape))
            {
                Exit ();
            }
            if (Keyboard.GetState ().IsKeyDown (Keys.S))
            {
                world.Save ();
            }
            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);

            // Disable anti-aliasing
            // https://gamedev.stackexchange.com/q/102681
            spriteBatch.Begin (SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

            renderer.Draw (GraphicsDevice.Viewport.TitleSafeArea, spriteBatch, world);

            spriteBatch.End ();

            base.Draw(gameTime);
        }
    }
}

