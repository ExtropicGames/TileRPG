﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

using TileRPG.Model;

namespace TileRPG.Shared
{
    public class TileResolver
    {
        private static readonly Dictionary<Map.Terrain, Point<int>> terrainLookup = new Dictionary<Map.Terrain, Point<int>>
        {
            { Map.Terrain.None, new Point<int>(0, 5) },
            { Map.Terrain.Dirt, new Point<int>(6, 0) },
            { Map.Terrain.Grass, new Point<int>(5, 0) },
        };

        // TODO: tell it which tileset to use
        public static Point<int> terrainIndexLookup(List<Map.Terrain> terrain)
        {
            Contract.Requires (terrain.Count == 9);
            Contract.Ensures (Contract.Result<Point<int>> ().x >= 0);
            Contract.Ensures (Contract.Result<Point<int>> ().y >= 0);

            // calculate water/land boundaries
            if (terrain [4] == Map.Terrain.Water)
            {
                int x = 0;
                int y = 0;
                // count how many water tiles are surrounding the center tile
                int count = (terrain [1] == Map.Terrain.Water ? 1 : 0) +
                            (terrain [3] == Map.Terrain.Water ? 1 : 0) +
                            (terrain [5] == Map.Terrain.Water ? 1 : 0) +
                            (terrain [7] == Map.Terrain.Water ? 1 : 0);

                // inlet
                if (count == 1)
                {
                    y += 1;
                    if (terrain [1] == Map.Terrain.Water)
                    {
                        x += 1;
                    }
                    else if (terrain [3] == Map.Terrain.Water)
                    {
                        x += 1;
                        y += 1;
                    }
                    else if (terrain [5] == Map.Terrain.Water)
                    {
                        y += 1;
                    }
                }
                //012
                //345
                //678
                // corner
                else if (count == 2)
                {
                    y = 3;

                    if (terrain [3] != terrain [5])
                    {
                        y += 1;

                        if (terrain [1] == Map.Terrain.Water)
                        {
                            y += 1;
                        }
                    }

                    if (terrain [3] == Map.Terrain.Water)
                    {
                        x += 1;
                    }

                    if ((
                        terrain [5] == Map.Terrain.Water &&
                        terrain [7] == Map.Terrain.Water &&
                        terrain [8] != Map.Terrain.Water
                    ) || (
                        terrain [3] == Map.Terrain.Water &&
                        terrain [7] == Map.Terrain.Water &&
                        terrain [6] != Map.Terrain.Water
                    ) || (
                        terrain [1] == Map.Terrain.Water &&
                        terrain [5] == Map.Terrain.Water &&
                        terrain [2] != Map.Terrain.Water
                    ) || (
                        terrain [1] == Map.Terrain.Water &&
                        terrain [3] == Map.Terrain.Water &&
                        terrain [0] != Map.Terrain.Water
                    ))
                    {
                        y += 2;
                    }
                }

                // coastline
                else if (count == 3)
                {
                    x = 2;

                    if (terrain [1] != Map.Terrain.Water)
                    {
                        x += (terrain [6] != Map.Terrain.Water ? 1 : 0);
                        y += (terrain [8] != Map.Terrain.Water ? 1 : 0);
                    }
                    else if (terrain [3] != Map.Terrain.Water)
                    {
                        x += (terrain [8] != Map.Terrain.Water ? 1 : 0);
                        y += (terrain [2] != Map.Terrain.Water ? 1 : 0) + 2;
                    }
                    else if (terrain [5] != Map.Terrain.Water)
                    {
                        x += (terrain [0] != Map.Terrain.Water ? 1 : 0);
                        y += (terrain [6] != Map.Terrain.Water ? 1 : 0) + 4;
                    }
                    else if (terrain [7] != Map.Terrain.Water)
                    {
                        x += (terrain [2] != Map.Terrain.Water ? 1 : 0);
                        y += (terrain [0] != Map.Terrain.Water ? 1 : 0) + 6;
                    }

                    return new Point<int> (x, y);
                }

                else if (count == 4)
                {
                    x = 4;

                    if (terrain[0] == Map.Terrain.Water)
                    {
                        x++;
                    }

                    if (terrain [2] == Map.Terrain.Water)
                    {
                        y += 4;
                    }

                    if (terrain [6] == Map.Terrain.Water)
                    {
                        y += 2;
                    }

                    if (terrain [8] == Map.Terrain.Water)
                    {
                        y++;
                    }
                }

                return new Point<int> (x, y);
            }

            return terrainLookup [terrain [4]];
        }
    }
}

