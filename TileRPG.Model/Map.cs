using System;
using System.Collections.Generic;

namespace TileRPG.Model
{
    [Serializable]
    public class Map
    {
        public enum Terrain
        {
            None, // special type, used to indicate edges of map
            Dirt,
            Water,
            Grass
        }

        public List<Creature> creatures;
        public Terrain[,] tiles;

        public Map (int width, int height)
        {
            creatures = new List<Creature>();
            tiles = new Terrain[width, height];

            Random r = new Random ();

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    tiles [x, y] = (Terrain) r.Next (2, 4);
                }
            }
        }

        public Terrain tile(int x, int y)
        {
            if (x < 0 || x >= tiles.GetLength (0))
            {
                return Terrain.None;
            }

            if (y < 0 || y >= tiles.GetLength (1))
            {
                return Terrain.None;
            }

            return tiles [x, y];
        }
    }
}

