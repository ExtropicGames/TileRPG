using System;
using System.Collections.Generic;
using System.IO;

using Newtonsoft.Json;

namespace TileRPG.Model
{
    [Serializable]
    public class World
    {
        public readonly List<Map> maps = new List<Map>();

        public void Save()
        {
            File.WriteAllText ("world.json", JsonConvert.SerializeObject (this));
        }

        public static World Load()
        {
            return JsonConvert.DeserializeObject<World> (File.ReadAllText ("world.json"));
        }
    }
}

