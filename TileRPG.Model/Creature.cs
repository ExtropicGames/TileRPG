using System;

namespace TileRPG.Model
{
    [Serializable]
    public class Creature
    {
        public enum Type
        {
            Human,
            Elf,
            Dwarf,
        }

        [NonSerialized]
        private Map map;

        public bool isPlayer;
        public Type type;
        public Point<int> position;

        public Creature (bool isPlayer = false)
        {
            this.isPlayer = isPlayer;
            type = Type.Human;
            position = new Point<int> (0, 0);
        }
    }
}

