#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using TileRPG.Shared;

#endregion

namespace TileRPG.Linux
{
    static class Program
    {
        private static TileGame game;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            game = new TileGame();
            game.Run();
        }
    }
}
