# Building

## OS X

- Make sure you have Xamarin Studio with Xamarin.Mac and the MonoGame add-in installed
- Open the .sln file and update the packages for TileRPG.Mac

# Notes

- Using [Kenney Game Assets](https://kenney.itch.io/kenney-donation) for graphics.
- Might consider [Kenney Game Assets 2](https://kenney.itch.io/kenney-game-assets-2) or [Kenney Studio](https://kenney.itch.io/kenney-studio)
